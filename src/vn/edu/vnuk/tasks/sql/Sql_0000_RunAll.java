package vn.edu.vnuk.tasks.sql;

import java.sql.Connection;
import java.sql.SQLException;

import vn.edu.vnuk.tasks.jdbc.ConnectionFactory;


public class Sql_0000_RunAll {

	public static void main(String[] args) throws SQLException {
		
		Connection connection = new ConnectionFactory().getConnection();
		
		new Sql_0010_CreateTasks(connection).run();
    }

}
